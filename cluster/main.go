package main

import (
	"context"
	"flag"
	"fmt"

	"strings"

	"pitayaServerDemo/middleware"
	"pitayaServerDemo/services"

	"github.com/topfreegames/pitaya/v2"
	"github.com/topfreegames/pitaya/v2/acceptor"
	"github.com/topfreegames/pitaya/v2/cluster"
	"github.com/topfreegames/pitaya/v2/component"
	"github.com/topfreegames/pitaya/v2/config"
	"github.com/topfreegames/pitaya/v2/conn/message"
	"github.com/topfreegames/pitaya/v2/groups"
	"github.com/topfreegames/pitaya/v2/route"
)

var app pitaya.Pitaya

func configureBackend() {
	room := services.NewRoom(app)
	app.Register(room,
		component.WithName("room"),
		component.WithNameFunc(strings.ToLower),
	)

	app.RegisterRemote(room,
		component.WithName("room"),
		component.WithNameFunc(strings.ToLower),
	)
}

func configureFrontend(port int) {
	app.Register(services.NewConnector(app),
		component.WithName("connector"),
		component.WithNameFunc(strings.ToLower),
	)

	app.RegisterRemote(services.NewConnectorRemote(app),
		component.WithName("connectorremote"),
		component.WithNameFunc(strings.ToLower),
	)

	err := app.AddRoute("room", func(
		ctx context.Context,
		route *route.Route,
		payload []byte,
		servers map[string]*cluster.Server,
	) (*cluster.Server, error) {
		//new
		s := app.GetSessionFromCtx(ctx)
		conn := s.RemoteAddr()
		s.Set("ip", conn.String())
		s.Set("route", route.String())
		s.Set("input", string(payload))
		//will return the first server
		for k := range servers {
			return servers[k], nil
		}
		return nil, nil
	})

	if err != nil {
		fmt.Printf("error adding route %s\n", err.Error())
	}

	err = app.SetDictionary(map[string]uint16{
		"connector.getsessiondata": 1,
		"connector.setsessiondata": 2,
		"room.room.getsessiondata": 3,
		"onMessage":                4,
		"onMembers":                5,
	})

	if err != nil {
		fmt.Printf("error setting route dictionary %s\n", err.Error())
	}
}

func main() {
	//new
	// port := flag.Int("port", 3250, "the port to listen")
	// svType := flag.String("type", "connector", "the server type")
	// isFrontend := flag.Bool("frontend", true, "if server is frontend")
	port := flag.Int("port", 3251, "the port to listen")
	svType := flag.String("type", "room", "the server type")
	isFrontend := flag.Bool("frontend", false, "if server is frontend")

	flag.Parse()

	builder := pitaya.NewDefaultBuilder(*isFrontend, *svType, pitaya.Cluster, map[string]string{}, *config.NewDefaultBuilderConfig())
	if *isFrontend {
		tcp := acceptor.NewTCPAcceptor(fmt.Sprintf(":%d", *port))
		builder.AddAcceptor(tcp)
	}
	builder.Groups = groups.NewMemoryGroupService(*config.NewDefaultMemoryGroupConfig())
	//new
	builder.HandlerHooks.BeforeHandler.PushBack(middleware.BeforeHandler_LoginCheck)
	builder.HandlerHooks.AfterHandler.PushBack(middleware.AfterHandler_Log)
	app = builder.Build()
	//new
	middleware.SetApp(app)

	//TODO: Oelze pitaya.SetSerializer(protobuf.NewSerializer())

	defer app.Shutdown()

	if !*isFrontend {
		configureBackend()
	} else {
		configureFrontend(*port)
	}

	var b byte = byte(message.Push)
	b = b << 1

	app.Start()
}
