package middleware

import (
	"context"
	"fmt"

	"github.com/topfreegames/pitaya/v2"
)

var exceptInRoute []string = []string{ // 这些路径不用验证是否登录
	"room.room.login",
}
var app pitaya.Pitaya

func SetApp(a pitaya.Pitaya) {
	app = a
}

// 未登录则要拦截下来，部分请求不用验证是否登录
func BeforeHandler_LoginCheck(ctx context.Context, in interface{}) (rsCtx context.Context, out interface{}, outErr error) {
	rsCtx = ctx
	out = in

	session := app.GetSessionFromCtx(ctx)
	route := session.Get("route")
	for _, rIn := range exceptInRoute {
		if rIn == route {
			return
		}
	}

	if session.UID() == "" {
		outErr = fmt.Errorf("未登录")
		return
	}
	return
}
