package middleware

import (
	"context"
	"fmt"
)

func AfterHandler_Log(ctx context.Context, in interface{}, inErr error) (out interface{}, outErr error) {
	outErr = inErr
	out = in

	session := app.GetSessionFromCtx(ctx)

	//模拟存入进log文件
	fmt.Printf("后置中间件:请求:%v\n", map[string]interface{}{
		"uid":   session.UID(),
		"ip":    session.Get("ip"),
		"route": session.Get("route"),
		"请求数据":  session.Get("input"),
		"响应数据":  in,
	})

	return
}
